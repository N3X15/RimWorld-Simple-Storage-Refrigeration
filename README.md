# Simple Storage: Refrigeration (Forked)

A fork of <https://github.com/JangoDsoul/Simply-Storage--Refrigeration> by the awesome JangoD'soul.

## Changes

* Fixed dependency issues with RimWorld <= 1.3
* Generated DDS files with RimPy
* Some cleanup
* Changed packageId and name to avoid making RimPy flip out.

## Downloading

You can either just download the zip file from [releases](https://gitlab.com/N3X15/RimWorld-Simple-Storage-Refrigeration/-/tags), the *bleeding-edge*, *buggy* [`main` branch](https://gitlab.com/N3X15/RimWorld-Simple-Storage-Refrigeration/-/archive/main/RimWorld-Simple-Storage-Refrigeration-main.zip), or you can download from git:

* Install [Git](https://git-scm.com/).
* Install [Git LFS](https://git-lfs.github.com/)
* Run `git clone git@gitlab.com:N3X15/RimWorld-Simple-Storage-Refrigeration.git <path/to/destination/folder>`

## License

Due to a lack of licensing information from JDS themselves, I've decided to go with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) - which is the most restrictive possible - until JDS says otherwise.

I'd prefer MIT, personally.

## Dev Stuff

Mostly reminders for myself, but also in case someone picks this up down the road.

### Deploying

Requires Python >= 3.10 and `poetry`

* Copy `bc7enc[.exe]` from RimPy/compressors/ to a new folder called `bin` under this directory.
* Run `poetry install --no-root`
* Run `poetry run python devtools/deploy.py [--deploy-to=/path/to/RimWorld/Mods/Simple_Storage_Refrigeration_Forked]`

Deployment files are in `dist/`, unless you used `--deploy-to`.