import os
from enum import IntEnum
from pathlib import Path
from typing import Any, Dict, FrozenSet, Iterable, List, Optional, Set

from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFileTarget
from PIL import Image

DIST_PATH: Path = Path("dist")

IGNORE_SUFFIXES: FrozenSet[str] = frozenset({".bak", "bak", ".dds", "dds"})
BC7ENC_PATH: Path = Path("bin") / "bc7enc"
if os.name == "nt":
    BC7ENC_PATH = BC7ENC_PATH.with_suffix(".exe")

os_utils.ensureDirExists(Path(".build") / "cache")

# https://github.com/richgel999/bc7enc_rdo/blob/e6990bc11829c072d9f9e37296f3335072aab4e4/test.cpp#L18

# bc7enc -1 -q -b -y -g
# -1: BC1
# -q: quiet
# -b: No 3-color texels on dark/black texels
# -y: Flip y before packing
# -g: Don't write unpackaged output PNGs

# bc7enc -u6 -y -g
# -uN: BC7 quality 1-6


class EBC7Quality(IntEnum):
    Q0 = 0
    Q1 = 1
    Q2 = 2
    Q3 = 3
    Q4 = 4
    Q5 = 5
    Q6 = 6
    LOWEST = 0
    HIGHEST = 6


def img_transparent(img: Image) -> bool:
    # https://stackoverflow.com/a/58567453
    if img.info.get("transparency", None) is not None:
        return True
    if img.mode == "P":
        transparent = img.info.get("transparency", -1)
        for _, index in img.getcolors():
            if index == transparent:
                return True
    elif img.mode == "RGBA":
        extrema = img.getextrema()
        if extrema[3][0] < 255:
            return True

    return False


class CompressDDSTarget(SingleBuildTarget):
    BT_LABEL: str = "BC7ENC"

    def __init__(
        self,
        bc7enc_path: Path,
        infile: Path,
        outfile: Path,
        dependencies: List[str] = [],
        is_transparent: Optional[bool] = None,
        quality: EBC7Quality = EBC7Quality.HIGHEST,
    ) -> None:
        self.bc7enc_path: Path = bc7enc_path
        self.infile: Path = infile
        self.outfile: Path = outfile
        self._is_transparent: Optional[bool] = is_transparent
        self.quality: EBC7Quality = quality

        super().__init__(
            target=str(outfile), files=[str(infile)], dependencies=dependencies
        )

    def get_config(self) -> Dict[str, Any]:
        cfg = super().get_config()
        cfg["bc7enc"] = str(self.bc7enc_path)
        cfg["infile"] = str(self.infile)
        cfg["outfile"] = str(self.outfile)
        cfg["istransparent"] = str(self._is_transparent)
        cfg["quality"] = str(self.quality.value)
        return cfg

    def get_opts(self) -> List[str]:
        # match mode:
        #     case 'RGB':
        #         return ['-1', '-q', '-b']
        #     case 'RGBA':
        #         return ['-u'+str(self.quality.value)]
        if self.is_transparent:
            # BC1 + no 3 color black + shut up
            return ["-1", "-b", "-q"]
        else:
            # BC7 quality n + shut up
            return ["-u" + str(self.quality.value), "-q"]

    @property
    def is_transparent(self) -> bool:
        if self._is_transparent is None:
            with self.infile.open("rb") as f:
                with Image.open(f) as img:
                    self._is_transparent = img_transparent(img)
        return self._is_transparent

    def build(self) -> None:
        os_utils.cmd(
            [str(self.bc7enc_path)]
            + self.get_opts()
            + ["-y", "-g", str(self.infile), str(self.outfile)],
            echo=self.should_echo_commands(),
            critical=True,
            globbify=False,
        )


def fileFilter(p: Path) -> bool:
    if not p.is_file():
        return False
    # print(p,p.suffix,p.suffix in IGNORE_SUFFIXES,p.name.endswith("~"))
    if p.suffix in IGNORE_SUFFIXES:
        # print('IS')
        return False
    if p.name.endswith("~"):
        # print('~')
        return False
    return True


toCopy: Set[Path] = set()


def markForCopy(parent: Path, globs: Optional[Iterable[str]] = None) -> None:
    global toCopy
    if globs is None:
        toCopy.update(filter(fileFilter, parent.absolute().rglob("*")))
    else:
        for glob in globs:
            toCopy.update(filter(fileFilter, parent.absolute().rglob(glob)))


def genDDSTasks(parent: Path) -> List[str]:
    o = []
    for fn in filter(fileFilter, parent.rglob("*")):
        relpath = fn.absolute().relative_to(Path.cwd())
        destpath = DIST_PATH / relpath
        match fn.suffix:
            case ".png" | "png":
                bc7enc_task = bm.add(
                    CompressDDSTarget(BC7ENC_PATH, fn, fn.with_suffix(".dds"))
                ).target
                o.append(
                    bm.add(
                        CopyFileTarget(
                            destfile=str(destpath),
                            sourcefile=str(fn),
                            dependencies=[bc7enc_task],
                        )
                    ).target
                )
                o.append(
                    bm.add(
                        CopyFileTarget(
                            destfile=str(destpath.with_suffix(".dds")),
                            sourcefile=str(fn.with_suffix(".dds")),
                            dependencies=[bc7enc_task],
                        )
                    ).target
                )
    return o


def existingDirPath(instr: str) -> Path:
    p = Path(instr)
    assert p.is_dir(), f"{p} does not exist, or is not a directory."
    return p


bm = BuildMaestro()
argp = bm.build_argparser()
argp.add_argument(
    "--deploy-to",
    type=existingDirPath,
    default=Path("dist"),
    help="Where to put the packaged mod",
)
args = bm.parse_args(argp)
DIST_PATH = args.deploy_to

markForCopy(Path("About"))
markForCopy(Path("Defs"))
markForCopy(Path("Patch"))
# markForCopy(Path("Textures"))


dds_tasks = genDDSTasks(Path("Textures"))

for fn in toCopy:
    relpath = fn.relative_to(Path.cwd())
    destpath = DIST_PATH / relpath
    bm.add(
        CopyFileTarget(
            destfile=str(destpath), sourcefile=str(fn), dependencies=dds_tasks
        )
    )

bm.as_app(argp)
